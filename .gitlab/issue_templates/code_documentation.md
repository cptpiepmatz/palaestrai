# Code Documentation Issue

<!-- PLEASE ADD APPROPRIATE PRIORITY, TYPE, AND CATEGORY LABELS -->

## Description

Issue Description: The code documentation for the class <!--Class Name--> is not sufficient.

## Missing Documentation

<!--What needs to be documented and is currently not documented -->

## Documentation needs to be edited

<!--Which documentation needs to be edited -->

## Order to Approach

<!-- How to best attempt the issue -->

## Estimated Weight / Value

<!-- How much time may be needed to fulfill the task before asking for help, and importance to the project as a whole. -->

## Acceptance Criteria for Merge

<!-- What is needed for a merge request-->

/label ~Documentation
