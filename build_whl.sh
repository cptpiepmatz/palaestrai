#!/bin/bash

# better logging
set -xv

# Source: https://stackoverflow.com/a/246128
SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

# Disable git dependencies for pypi release, since they don't allow them
sed -i 's/.*extras.update({"full-dev": full_dev}).*/# extras.update({"full-dev": full_dev})/g' "${SCRIPT_DIR}/setup.py"

if [ "$BUILD_TYPE" == "dev" ]; then
  # Change version number to contain dev-tag
  TMP_VERSION="$(cat "${SCRIPT_DIR}/src/palaestrai/version.py")"

  DATE=$(date '+%Y%m%d%H%M')
  DEV_VERSION=$(sed "s/ //g;s/\"//g;/__version__/s/$/.dev${DATE}/;s/__version__=//g" "${SCRIPT_DIR}/src/palaestrai/version.py")
  echo "__version__ = \"${DEV_VERSION}\"" > "${SCRIPT_DIR}/src/palaestrai/version.py"
fi

python3 -m pip install -U twine build

tmp_dir="$(mktemp -d -t)"

# Sources:
# https://stackoverflow.com/a/58968358
# https://stackoverflow.com/a/52575087
# This command builds the package and outputs the command output regualarily
# to have it observable in the logs. In the same time, it asynchronously
# performs a filter on the stderr of the build. It outputs
# "Package would be ignored" to stderr if e.g. an __init__.py is missing and
# thus the subdir is ignored in the built wheel. This should be noticed and
# handled manually for each individual case. So the pipeline should exit
# with an error
{ package_ignored="$(python3 -m build "${SCRIPT_DIR}" 2> >(tee >(grep "Package would be ignored" -c) >&3) >&2)"; echo "$package_ignored" > "$tmp_dir/package_ignored"; } 3>&1

# Enable again
sed -i 's/.*extras.update({"full-dev": full_dev}).*/extras.update({"full-dev": full_dev})/g' "${SCRIPT_DIR}/setup.py"

if [ "$BUILD_TYPE" == "dev" ]; then
  # Rollback version number
  echo "${TMP_VERSION}" > "${SCRIPT_DIR}/src/palaestrai/version.py"
fi

# Get counted ignored packages
package_ignored="$(cat "$tmp_dir/package_ignored")"

# Source: https://stackoverflow.com/a/806923
re='^[0-9]+$'
if ! [[ "$package_ignored" =~ $re ]] ; then
   echo "error: 'package_ignored' not a number" >&2; exit 1
fi

# Check if packages are ignored, based on the filter on the stderr of python
# wheel building
if [ "$package_ignored" -ne 0 ]; then
   echo "error: 'package_ignored' != 0: There are packages, that would be ignored for uploading wheel!" >&2; exit 1
fi

rm -r "$tmp_dir"