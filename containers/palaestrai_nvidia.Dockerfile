# palaestrai -- Full-stack palaestrAI dockerfile
#
# The base image contains the extended Python basis along with the packages
# palaestrAI needs in any case. Derived versions either check out palaestrAI
# or install it from PyPI, depending on the "BUILD_TYPE" argument.
#
# BUILD_TYPE:
#   BUILD_TYPE=development  Check out current development from git
#   BUILD_TYPE=master       Install palaestrai & packages from PyPI
#
# IMAGE_VERSION:
#   Sets the version of the Nvidia base image as
#   `FROM nvidia/cuda:$IMAGE_VERSION`
#
# BUILD_BASE_TESTING:
#   Base image for the "testing" build type. Defaults to "development", but
#   can also be a completely different base image. Useful for referring to a
#   common base image, such as `palaestrai-base:development`.
#

ARG BUILD_TYPE=development
ARG IMAGE_VERSION=12.2.2-base-ubuntu22.04
ARG BUILD_BASE_MASTER=base
ARG BUILD_BASE_TESTING=base

FROM nvidia/cuda:$IMAGE_VERSION AS base
ENV TZ=UTC
ENV DEBIAN_FRONTEND=noninteractive

RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime \
    && echo "$TZ" > /etc/timezone \
    && apt-get clean \
    && apt-get update -y \
    && apt-get upgrade -y \
    && apt-get install -y --no-install-recommends \
        wget \
        git \
        vim \
        htop \
        sudo \
        curl \
        sqlite3 \
        postgresql-client \
        python3 \
        python3-pip \
        python3-dev \
        build-essential \
        pandoc \
        graphviz \
        graphviz-dev \
        libxml2-dev \
        libxslt-dev \
    && python3 -m pip install -U pip \
    && useradd -Um -G users palaestrai \
    && mkdir /palaestrai \
    && mkdir -p /workspace \
    && chown palaestrai:palaestrai /workspace \
    && chmod 1770 /workspace


FROM base AS development
WORKDIR /palaestrai
ENTRYPOINT ["/palaestrai/containers/start.sh"]


FROM $BUILD_BASE_TESTING AS testing
ENTRYPOINT []


FROM $BUILD_BASE_MASTER AS master
ENTRYPOINT ["/palaestrai/containers/start.sh"]


FROM $BUILD_TYPE AS final
WORKDIR /palaestrai
COPY [".", "/palaestrai/"]
# """
#    Another thing to be careful about is that after every FROM statement,
#    all the ARGs get collected and are no longer available.
# """
# To have an ARG available with the same value as before, they have to be
# "requested" to be present, see: https://stackoverflow.com/a/56748289
ARG BUILD_TYPE
RUN python3 -m pip install -UI -e . --prefer-binary
RUN python3 -m pip install -UI '.[full]' --prefer-binary
RUN \
    if [ "$BUILD_TYPE" = "development" ]; then \
      python3 -m pip install -UI -e '.[full-dev]' --prefer-binary; \
    fi
RUN chmod 0755 /palaestrai/containers/start.sh \
    && chown -R palaestrai:users /palaestrai \
    && ls -Al /palaestrai
RUN apt-get autoremove -y \
    && apt-get autoclean \
    && rm -rf /var/lib/apt/lists/* \
    && rm -rf /tmp/{palaestrai*,harl,arsenai,*mosaik*}
WORKDIR /workspace
