RunGovernor state diagram
=========================

.. image:: ../_static/rungov_state_diagram.svg

The different RunGovernor states can be generalized into three different
groups which are also color coded in the diagram.

1. Initializing and starting the experiment run (green)
2. Shutting down the experiment run and clean up (blue)
3. Error handling (orange)


Initialization and Startup
--------------------------

When a new instance of the RunGovernor gets launched by the Executor it always
gets launched as ``RGSPristine`` which as the name suggests resembles a fresh
and unused RunGovernor this state however is almost instantly changed to
``RGSInitializing`` and then to ``RGSTransceiving``. During a normal
experiment run the RunGovernor would now stay in this state till it receives
an ``ExperimentRunStartRequest`` from the Executor upon receiving it the
RunGovernor will attempt to start the experiment run and changes its state to
``RGSStartingSetup`` during this state the RunGovernor checks if it can run
the experiment at all and if it's running already when it can run the
experiment its state changes to ``RGSStartingSimControllers``,
``RGSStartingEnvConductors`` and ``RGSStartingAgentConductors`` in that order
to create all processes for SimulationControllers, EnvConductors and
AgentConductors that are needed for the experiment run. Once all those
processes are created the RunGovernor changes states to ``RGSStartingRun``
which simply starts all previously created processes. At this point the
initialization and start up of the experiment run are done and the RunGovernor
changes back into the ``RGSTransceiving`` state.

Shutdown and clean up
---------------------

If a RunGovernor in the state ``RGSTransceiving`` receives an
``ExperimentRunShutdownRequest`` it will change its state to
``RGSStoppingSimulation`` and then send a ``SimulationStopRequest`` to the
SimulationController of the experiment run the SimulationController now
handles the shutdown of all Conductors, Enviornments and Agents and responds
with a ``SimulationStopResponse``. The RunGovernor then shuts down the
SimulationController and sends an ``ExperimentRunShutdownResponse`` to the
Executor afterwards it changes its state to ``RGSStoppingTransceiving`` which
stops it from receiving new requests, then it enters the state
``RGSStoppingRun`` which stops all remaining processes of Conductors and
SimulationControllers, if the experiment has a next phase the RunGovernor will
now enter ``RGSStartingSetup`` again. Afterwards the states ``RGSFinalizing``
and ``RGSDone`` are entered.  Finalizing terminates all subprocesses that
might still run and with RGSDone the RunGovernor terminates itself.


Error handling
--------------

The plethora of errors that can occur while the RunGovernor runs are handled
by four different Error handling states. However only three of them do
actually have functionality at the moment since
``RGSErrorHandlingInitializing`` can not be entered. Another state
``RGSHandlingDeadChildren`` is only entered from the error state
``RGSErrorHandlingTransceiving`` and basically only checks if a premature task
death has occured, in this case the RunGovernor will jump straight into the
``RGSStoppingTransceiving`` state, or not in which case the RunGovernor will
initialize shutdown by entering the state ``RGSStoppingSimulation``.

Most errors that are handled in the state ``RGSErrorHandlingTransceiving``
will result in the RunGovernor reentering the state ``RGSTransceiving`` with
the exception of ``SignalInterruptError``, which will cause the RunGovernor to
stop the current experiment phase by entering ``RGSStoppingSimulation``, and
``DeadChildrenRisingAsZombiesError``, which lets the RunGovernor switch to the
``RGSHandlingDeadChildren`` state.

The state ``RGSErrorHandlingStarting`` causes the RunGovernor to either change
its state back to ``RGSTransceiving`` if the error that was caught was an
``ExperimentAlreadyRunningError`` or to ``RGSStoppingTransceiving`` if the
error was an ``ExperimentSetupFailedError`` all other errors that are handled
by ``RGSErrorHandlingStarting`` will let the RunGovernor change to the
``RGSStoppingSimulation`` state.
