Memory
======

The :class:`~Memory` is a Class used in the:class:`~Brain` to store information. The basic :class:`~Memory` e.g., stores all received sensor information,
all actuator values set by the agent, and the reward returned by the environment(s). Additionally, it stores the internal
reward calculated by the objective.

Memory as Replay Buffer
-----------------------
The :class:`~Memory` can be used as a replay buffer. The base class can be extended by additional functions and attributes if needed.



How to use a specific memory
----------------------------
Since :class:`~Memory` is initialized in the :class:`~Brain`, memory can be defined as a parameter of the :class:`~Brain`. For this, the path to the :class:`~Memory` class is passed to the params of the
:class:`~Brain` class. Additionally, the parameters for the :class:`~Memory` can be defined. For example:

.. code-block::

   name: mighty_defender
    brain:
     name: palaestrai.agent.dummy_brain:DummyBrain
     params:
       params:
         memory_class: palaestrai.agent.memory:Memory
         memory_params: {}


.. warning::

    The base lists (env_rewards, actions, observations, internal_rewards) are automatically filled in the brain. Do not add values to it, this could result in duplicates. You can manipulate existing values if needed or add
    values which are not part of the :class:`~MuscleUpdateRequest`. The only exception is the additional data which have to
    be implemented individually.



API Documentation
-----------------

Memory
~~~~~~

.. autoclass:: palaestrai.agent.Memory
    :members:
    :noindex:
