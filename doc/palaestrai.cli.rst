palaestrai.cli package
======================

Submodules
----------

palaestrai.cli.client\_local module
-----------------------------------

.. automodule:: palaestrai.cli.client_local
   :members:
   :undoc-members:
   :show-inheritance:

palaestrai.cli.manager module
-----------------------------

.. automodule:: palaestrai.cli.manager
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: palaestrai.cli
   :members:
   :undoc-members:
   :show-inheritance:
