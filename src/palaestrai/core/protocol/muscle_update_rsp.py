from __future__ import annotations
from typing import Any

from dataclasses import dataclass


@dataclass
class MuscleUpdateResponse:
    """Result of a :class:`Brain`'s working

    * Sender: :class:`Learner`
    * Receiver: :class:`RolloutWorker`

    Upon receiving the corresponding :class:`MuscleUpdateRequest` message,
    the :class:`Leaner` will query its :class:`Brain` for new parameters for
    the particular :class:`Muscle`.
    What is sent depends on the algorithm.

    Parameters
    ----------

    sender_brain_id : str
        The brain the update comes from
    receiver_muscle_id : str
        The :class:`Muscle` that potentially receives an update
    experiment_run_id : str
        ID of the current experiment run this environment participates in
    experiment_run_instance_id : str
        ID of the :class:`~ExperimentRun` object instance
    experiment_run_phase : int
        Current phase number of the experiment run
    update : Any
        Whatever a :class:`Brain` wants to send to a :class:`Muscle`. This can
        be ``None``, in which case there is no update. However, since the
        major domo protocol's request-response pattern requires a response to
        *any* request, empty updates must be sent.
    """

    sender_brain_id: str
    receiver_muscle_id: str
    experiment_run_id: str
    experiment_run_instance_id: str
    experiment_run_phase: int
    update: Any = None

    @property
    def sender(self):
        return self.sender_brain_id

    @property
    def receiver(self):
        return self.receiver_muscle_id

    def has_update(self):
        return self.update is not None
