from __future__ import annotations
from .shutdown_rsp import ShutdownResponse


class SimulationShutdownResponse(ShutdownResponse):
    pass
