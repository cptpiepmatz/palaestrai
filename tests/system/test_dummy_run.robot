*** Settings ***
Documentation   Test Dummy Run
...
...             Exercises the fully dummy run, checking for all sorts of
...             details of the run execution. It does not check for results
...             storage explicitly, but will make sure that all log outputs
...             indicate a safe, successful and complete execution of the
...             dummy experiment run.

Library         String
Library         Process
Library         OperatingSystem
Library         ${CURDIR}${/}ConfigFileModifier.py
Suite Teardown  Clean Files
Suite Setup     Create Config Files

*** Keywords ***
Clean Files
    Remove File                     ${TEMPDIR}${/}stdout_dummy_experiment.txt
    Remove File                     ${TEMPDIR}${/}stderr_dummy_experiment.txt
    Remove File                     ${db_file_path}
    remove file                     ${runtime_config_file}

Create Config Files
    ${result} =                     Run Process   palaestrai  runtime-config-show-default  stdout=${TEMPDIR}${/}palaestrai-default-runtime-dummyrun.conf.yml
    ${queueidx} =                   Get Variable Value  ${PABOTQUEUEINDEX}  0
    ${LOGPORT}                      Evaluate    str(24243 + random.randrange(1000 * (${PABOTQUEUEINDEX}+1)))
    ${EXECUTORPORT}                 Evaluate    str(24242 - random.randrange(1000 * (${PABOTQUEUEINDEX}+1)))
    ${conf} =                       Replace String  ${result.stdout}  4242  ${EXECUTORPORT}
    ${conf} =                       Replace String  ${conf}  4243  ${LOGPORT}
    ${conf} =                       Replace String  ${conf}  palaestrai.db  palaestrai-dummyrun.db
    Set Suite Variable              $runtime_config_file  ${TEMPDIR}${/}dummyrun-test-${LOGPORT}${EXECUTORPORT}.conf.yml
    Create File                     ${runtime_config_file}.old  ${conf}
    ${db_file_path} =               prepare_for_sqlite_store_test   ${runtime_config_file}.old  ${runtime_config_file}  ${TEMPDIR}
    Set Suite Variable              $db_file_path
    Log File                        ${runtime_config_file}


*** Test Cases ***
Run dummy experiment
    [Timeout]                       300
    ${result} =                     Run Process   palaestrai  -vv  -c  ${runtime_config_file}   database-create  stdout=stdout_dummy_experiment.txt  stderr=stderr_dummy_experiment.txt
    Log Many                        ${result.stdout}    ${result.stderr}
    Should Be Equal As Integers     ${result.rc}    0
    ${result} =                     Run Process   palaestrai  -vv  -c  ${runtime_config_file}  experiment-start  ${CURDIR}${/}..${/}fixtures${/}dummy_run.yml  stdout=${TEMPDIR}${/}stdout_dummy_experiment.txt  stderr=${TEMPDIR}${/}stderr_dummy_experiment.txt  cwd=${TEMPDIR}
    Log Many                        ${result.stdout}  ${result.stderr}
    Should Be Equal As Integers     ${result.rc}  0
    Should Contain                  ${result.stdout}  set up 2 AgentConductor object(s)
    File Should Exist               ${TEMPDIR}${/}_outputs${/}brains/Yo-ho, a dummy experiment run for me!${/}0${/}mighty_defender.bin
    File Should Exist               ${TEMPDIR}${/}_outputs${/}brains/Yo-ho, a dummy experiment run for me!${/}0${/}evil_attacker.bin
    File Should Exist               ${TEMPDIR}${/}_outputs${/}brains/Yo-ho, a dummy experiment run for me!${/}1${/}mighty_defender.bin
    File Should Exist               ${TEMPDIR}${/}_outputs${/}brains/Yo-ho, a dummy experiment run for me!${/}1${/}evil_attacker.bin

Run dummy experiment with Taking Turns Simulation Controller
    [Timeout]                       300
    ${result} =                     Run Process   palaestrai  -vv  -c  ${runtime_config_file}   database-create  stdout=stdout_dummy_experiment.txt  stderr=stderr_dummy_experiment.txt
    Log Many                        ${result.stdout}    ${result.stderr}
    Should Be Equal As Integers     ${result.rc}    0
    ${result} =                     Run Process   palaestrai  -vv  -c  ${runtime_config_file}  experiment-start  ${CURDIR}${/}..${/}fixtures${/}dummy_run_taking_turns.yml  stdout=${TEMPDIR}${/}stdout_dummy_experiment.txt  stderr=${TEMPDIR}${/}stderr_dummy_experiment.txt  cwd=${TEMPDIR}
    Log Many                        ${result.stdout}  ${result.stderr}
    ${result} =                     Run Process  sqlite3  ${db_file_path}  WITH last_erp(id) AS (SELECT MAX(id) FROM experiment_run_phases), ma AS (SELECT *, LAG(muscle_actions.agent_id, 1, 0) OVER (ORDER BY muscle_actions.id) AS previous_agent_id FROM muscle_actions, last_erp JOIN agents ON muscle_actions.agent_id \= agents.id JOIN main.experiment_run_phases erp ON agents.experiment_run_phase_id \= erp.id WHERE erp.id \= last_erp.id) SELECT SUM(ma.agent_id \= ma.previous_agent_id) AS consecutive_updates FROM ma GROUP BY ma.experiment_run_phase_id;
    Log Many                        ${result.stdout}  ${result.stderr}
    Should Be Equal As Integers     ${result.rc}  0
    Should Be Equal As Integers     ${result.stdout}  0
